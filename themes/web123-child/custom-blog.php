<?php
/**
* Template Name: Blog Main
*
*/
get_header(); ?>
<div class="slider_holder">
  <?php echo do_shortcode('[rev_slider alias="questslider"]'); ?>
</div>



<div class="custom__blogs_h grid_section">
<div class="left_bar_custom section_inner">

<?php
           $args = array('post_type' => 'post', 'posts_per_page' => -1, 'order' => 'ASC' );
           $mypost = new WP_Query($args);
           global $post;
           $posts = $mypost->get_posts();
           foreach ($posts as $post) {
//echo "<pre>"; print_r($post);
           	?>

<div class="i001-list-item cms-mg-obj">
<div class="i001-list-wrap">
<h4>
<a href="<?php echo get_post_permalink($post->ID); ?>"><?php echo $post->post_title ?></a>
</h4>

<?php
$content = $post->post_content;
$content = preg_replace("/<img[^>]+\>/i", " ", $content);
$content = apply_filters('the_content', $content);
$content = str_replace(']]>', ']]>', $content);
echo wp_trim_words( $content, 40, '...' ); ?>

<p>
<?php echo '<a type="button"  href="' . get_post_permalink($post->ID) . '" class="i001-css-button new_v01">Read More</a>'; ?>
</p>


</div>
</div>
<?php } ?>
</div>
<div class="right__side_bar">
	<!-- <?php echo dynamic_sidebar('Sidebar'); ?> -->
</div>
</div>

<?php get_footer(); ?>
